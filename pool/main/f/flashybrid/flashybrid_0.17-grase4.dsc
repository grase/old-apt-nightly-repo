-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: flashybrid
Binary: flashybrid
Architecture: all
Version: 0.17-grase4
Maintainer: Debian QA Group <packages@qa.debian.org>
Homepage: http://www.xorcom.com/rapid/
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5.0.0), po-debconf
Package-List: 
 flashybrid deb admin extra
Checksums-Sha1: 
 0d6e9a62b771f08c5990ae44ebc65443efa5ed6d 30015 flashybrid_0.17-grase4.tar.gz
Checksums-Sha256: 
 e9831fd794786e55452a3c999507c15197d24a3edc2888d5fd8ae733420877e4 30015 flashybrid_0.17-grase4.tar.gz
Files: 
 3550e0be143af8a677d61e05f92f41bd 30015 flashybrid_0.17-grase4.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQEcBAEBAgAGBQJREy4QAAoJECDjIQu1nSa9c9sIAK/QWL3wnCk4/7wcem/qvLTx
gNi3WAi+QDyPNwLNo+XLuk4UqtCGFxxyKnOBDnVLIXL1TW9zMvBcy/MAkNr2q9LZ
j7mpGQvgFokzMJarEJa/9HH1/1bC3+oPxCpvkZes19BQATIlAS7vY8QQ4piOLV7v
CJDoRNV81YVnhilnm6sxxhgVgoMIp3gI/VMDQcDgfxA8BknOOiBZbaBFQlMEed99
99rIUo85A222SX26rfilA1fx1XSS42ulq8jf86naHnNRVpcHjc4+XsmPW0eYIMqA
KltiRk6owAPauYizHN6v8LJzsZQs29FQtoLsE4o/+1MkUamfXPbtjEyc/4r1WNg=
=Wh+N
-----END PGP SIGNATURE-----
