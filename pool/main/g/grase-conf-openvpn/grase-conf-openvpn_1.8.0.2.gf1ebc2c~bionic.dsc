Format: 3.0 (native)
Source: grase-conf-openvpn
Binary: grase-conf-openvpn
Architecture: all
Version: 1.8.0.2.gf1ebc2c~bionic
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.8.4
Build-Depends: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.2~)
Package-List:
 grase-conf-openvpn deb config extra arch=all
Checksums-Sha1:
 89302e22ab31f6c8416d04222a941f054ebeb4f8 22600 grase-conf-openvpn_1.8.0.2.gf1ebc2c~bionic.tar.xz
Checksums-Sha256:
 777b92bb087b0af2ee50e645ded4b0f8e1fd9112b7f1baa514fb5429a57b76e9 22600 grase-conf-openvpn_1.8.0.2.gf1ebc2c~bionic.tar.xz
Files:
 2d0762136e3122fa1b202d0a54203638 22600 grase-conf-openvpn_1.8.0.2.gf1ebc2c~bionic.tar.xz
