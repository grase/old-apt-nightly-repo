-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: grase-remote-assist-user
Binary: grase-remote-assist-user
Architecture: all
Version: 1.5
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.8.4
Build-Depends: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.0~)
Package-List:
 grase-remote-assist-user deb config extra arch=all
Checksums-Sha1:
 a8504f3740927855a3e3328419fb059c7d7a0653 3056 grase-remote-assist-user_1.5.tar.xz
Checksums-Sha256:
 630d413f86c0931a0ce422d8b85005b2943109ef2505bba37e4afea573f72a9d 3056 grase-remote-assist-user_1.5.tar.xz
Files:
 f720b2de94932625ebbe2efffafba7ff 3056 grase-remote-assist-user_1.5.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJUO5xdAAoJEEMVeDnIMO1m6WsH/3JoJMgF144eDEAuIaWHpORc
oiIjvSYfEergdLODZty4fi2qEwxl3jFHxrRbMJT1bMBL6b/Dl6hAQczjHnY5H1sd
aockGVMrdjn7cY8eOc8WfsWxvKNmyWPmEJcZw2Fx8VR2/o3KcqQQ/1rrD3k+1aIn
sfYfEneXJtoVjofQbqboilIW0GCDM3EaQ3oFNkVDTklJHOCNdrFMARSefcz7YoF8
s04MIJjJG5sDRj2mwlzQbeemZ8LN6kzRw5wGeIuoB9MhphvDa1QETqB9koQHc/sz
FuXHRJfOx3GYjU8Z3DBLCkJWW81H1P38EEEuMfFe5d6T+4m1k94JnD9GaJeVIMQ=
=igoG
-----END PGP SIGNATURE-----
