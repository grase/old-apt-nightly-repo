-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: grase-repo
Binary: grase-repo
Architecture: all
Version: 1.6
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.9.2
Build-Depends: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.0~)
Package-List:
 grase-repo deb config extra arch=all
Checksums-Sha1:
 6bcb9c702a42c5b5a419f055c1ec2a88536a81cd 4428 grase-repo_1.6.tar.xz
Checksums-Sha256:
 013b8e1a6d3c2b4aa3b367479368e75a2a1ec5f3c9558ccc4b6dc67fe6b7b375 4428 grase-repo_1.6.tar.xz
Files:
 96306b997f0da2d39c4ffe235db70461 4428 grase-repo_1.6.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJWPJInAAoJEEMVeDnIMO1myWUH/AphxsMsgqnLkYSaARE/H9O/
uPvXt50ikTkzTLv1yOB6KgronMzGhhQw2mJ4hL0kzm816uywUAhll5lf+y7KRI7y
EOxMC+jQ3kpd5ajTCDb3LrdJFWlGvWHhz9MzC1icHiAVNuAJi17CvT4U+amVoFCr
zAbc7qZfr330D9QJ7TKnuZf+TwNzvtB4jzrffeRd0mokU8kI/w/1PSS9dETmSatR
kmDJA/5UZXjgw/wSLWzk5RdC/KaxwNv8iIK01xiGX40vygqZSvZ+q6ixEuEz/sVB
HQjKJHIG0Wz3940M4wPmX0Pef2P/JSezOigMKZmU/NsZwAG7R46ayMbvaUp2RZI=
=C5Yq
-----END PGP SIGNATURE-----
