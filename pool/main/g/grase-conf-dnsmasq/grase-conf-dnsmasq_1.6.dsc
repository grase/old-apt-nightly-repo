Format: 1.0
Source: grase-conf-dnsmasq
Binary: grase-conf-dnsmasq
Architecture: all
Version: 1.6
Maintainer: Tim White <tim@hotspot.purewhite.id.au>
Homepage: http://grasehotspot.org/
Standards-Version: 3.9.1
Build-Depends-Indep: cdbs (>= 0.4.85~), debhelper (>= 7.0.1), dh-buildinfo, config-package-dev (>= 4.5~)
Checksums-Sha1: 
 adbd39516472d54caec4f348e1c1b9b687238cdc 3518 grase-conf-dnsmasq_1.6.tar.gz
Checksums-Sha256: 
 d417bf94272eb2efed80e57f891f06ed63ae4d32445995acdc377cec5e848f1c 3518 grase-conf-dnsmasq_1.6.tar.gz
Files: 
 f38132dd5565e2e6c22d8c1f06b588bf 3518 grase-conf-dnsmasq_1.6.tar.gz
