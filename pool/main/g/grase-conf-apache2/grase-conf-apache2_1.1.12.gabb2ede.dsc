-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: grase-conf-apache2
Binary: grase-conf-apache2
Architecture: all
Version: 1.1.12.gabb2ede
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.9.1
Build-Depends-Indep: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.0~)
Package-List:
 grase-conf-apache2 deb config extra arch=all
Checksums-Sha1:
 4a19efa3842778b6bb9f2e0ff38d3891c0fa2e53 3195 grase-conf-apache2_1.1.12.gabb2ede.tar.gz
Checksums-Sha256:
 eb6d6a6a197cf76fcd5f4b65e371c8f7466439f95da0e8f8c498047669821a69 3195 grase-conf-apache2_1.1.12.gabb2ede.tar.gz
Files:
 3d9d273a7fd50255fa89554938ff7397 3195 grase-conf-apache2_1.1.12.gabb2ede.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJUA5kEAAoJEEMVeDnIMO1mZC8H/jfq7TRJCb/QR1/2Eg5J+LqI
Qdc2QCD38XCohJF40VxbfQVj7qS3xSAVyLAmLtK6LRF8BOHh2xGZU23vudawEUWB
l2N12dIVOYMH74kZxvWswjYvl80mzwcxZ0zI1oRJCev/xrrE3SnWh5ZSsPmZR8mb
hW6vbxsldPsXrIaonV0uNHgUPirWjtKGVJb/ONIzFr/18N3p04Qnb257Kw+hK4L8
Ii/A/RZC62WTtIbf9XeezbL8nmwk4U13YyWgnnBQZgD23DDEM3IY9PGbIfSeGV7/
CHPEwujd9Z/dYwy8/9V+iDxfSSHZPJHdItknST7M7ZcSxSqxKT6IT4O9mN+L+o8=
=gF51
-----END PGP SIGNATURE-----
