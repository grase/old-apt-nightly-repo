-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: grase-www-portal
Binary: grase-www-portal, grase-www-portal-ext-libs
Architecture: all
Version: 3.8.0.36.g011c749
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.9.1
Build-Depends: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.0~)
Package-List:
 grase-www-portal deb net extra arch=all
 grase-www-portal-ext-libs deb net extra arch=all
Checksums-Sha1:
 93feefbbf441d420f5ab38ff97a54286957361dc 524684 grase-www-portal_3.8.0.36.g011c749.tar.xz
Checksums-Sha256:
 fb603aec43fb5b5201c2c816c3d16bb01693b97df40dec2c5bd12e39ea7664c9 524684 grase-www-portal_3.8.0.36.g011c749.tar.xz
Files:
 5f92d0e3fa9e2b4f8156a799a7ddc02f 524684 grase-www-portal_3.8.0.36.g011c749.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJX2oGrAAoJEEMVeDnIMO1m6YgH/2jF3IfMdqGCHsU2TmZLa4Hb
CFTBS4drTpqRa04qR2Znzl4Y/ggBUdHtFySf/SFF5nucZrcTWaxphC6J+8Z7LjVB
Xe79eUUHtXWRHCoNZ6yd6YmkfKDcLgi91gVVzCnsm/ZGTC57fyBtHhkUSYrSer9l
awxrjMGeY7xd5a6Lw3N+CQFI7OQDuaZbQ5zBiR5ygxwpY5dXbQAO4RxCHzH6U22e
S420mlloVfO8bYusS4pusL5lrAUGugt9qELuqeZUtf0jzaMWA/XpnIAkeM27A4zq
BwtLf8Z1mXoUngpc6g3yqIzurb5ZoQSW8mg97jzMUvY+gbEolnapz26vZgexEMc=
=xR28
-----END PGP SIGNATURE-----
